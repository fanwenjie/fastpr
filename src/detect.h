//
//  detect.h
//  FastPRDemo
//
//  Created by fanwenjie on 2017/6/3.
//  Copyright © 2017年 fanwenjie. All rights reserved.
//

#ifndef locate_h
#define locate_h

#include <stdio.h>
#include "utility.h"
///检测图片
Rect Detect(const unsigned char *grayImage, const short width, const short height);

#endif /* locate_h */
