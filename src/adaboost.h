//
//  adaboost.hpp
//  FastPR
//
//  Created by fanwenjie on 2017/2/4.
//  Copyright © 2017年 fanwenjie. All rights reserved.
//

#ifndef adaboost_h
#define adaboost_h

#include "utility.h"

#include "adaboost.gen.h"

#ifdef __cplusplus
extern "C" {
#endif
///目标检测
Rect ObjectDetect(const unsigned char *grayImage,const short width,const short height);

        
#ifdef __cplusplus
}
#endif

#endif /* adaboost_h */
