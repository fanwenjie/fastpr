//
//  recognize.h
//  FastPRDemo
//
//  Created by fanwenjie on 2017/6/6.
//  Copyright © 2017年 fanwenjie. All rights reserved.
//

#ifndef recognize_h
#define recognize_h
#ifdef _WIN32
#define API __stdcall __declspec(dllexport)
#else
#define API __attribute__((visibility("default")))
#endif
///识别图片
API char FastprRecognize(const unsigned char *grayImage, const short width, const short height, short outRegion[4],unsigned short outResult[8]);


#endif /* recognize_h */
