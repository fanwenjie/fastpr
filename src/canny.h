//
//  canny.h
//  FastPRDemo
//
//  Created by fanwenjie on 2017/6/1.
//  Copyright © 2017年 fanwenjie. All rights reserved.
//

#ifndef canny_h
#define canny_h
///Canny滤波
void Canny(unsigned char *src, unsigned char *dst, const int cols, const int rows, const int low_thresh, const int high_thresh);


#endif /* canny_h */
