#ifndef CNN_H
#define CNN_H

#include <stdio.h>

#define WIDTH_SUBSAMP	3
#define HEIGHT_SUBSAMP	2

#define WIDTH_INPUT		110
#define HEIGHT_INPUT	35
#define PADDING			2

#define WIDTH_KERNEL	9
#define	HEIGHT_KERNEL	6

#define WIDTH_FEATURE0	113
#define WIDTH_FEATURE1	(WIDTH_FEATURE0 - WIDTH_KERNEL + 1)
#define WIDTH_FEATURE2	(WIDTH_FEATURE1 / WIDTH_SUBSAMP)
#define WIDTH_FEATURE3	(WIDTH_FEATURE2 - WIDTH_KERNEL + 1)
#define	WIDTH_FEATURE4	(WIDTH_FEATURE3 / WIDTH_SUBSAMP)
#define WIDTH_FEATURE5	(WIDTH_FEATURE4 - WIDTH_KERNEL + 1)

#define HEIGHT_FEATURE0	39
#define HEIGHT_FEATURE1	(HEIGHT_FEATURE0 - HEIGHT_KERNEL + 1)
#define HEIGHT_FEATURE2	(HEIGHT_FEATURE1 / HEIGHT_SUBSAMP)
#define HEIGHT_FEATURE3	(HEIGHT_FEATURE2 - HEIGHT_KERNEL + 1)
#define	HEIGHT_FEATURE4	(HEIGHT_FEATURE3 / HEIGHT_SUBSAMP)
#define HEIGHT_FEATURE5	(HEIGHT_FEATURE4 - HEIGHT_KERNEL + 1)

#define LAYER0			1
#define LAYER1			8
#define LAYER2			8
#define LAYER3			16
#define LAYER4			16
#define LAYER5			120
#define OUTPUT          45

#define IMAGE_WIDTH 110
#define IMAGE_HEIGHT 35
#define SZPACK 8
#define PAD 2
typedef float pack_t[SZPACK];
typedef unsigned char uint8_t;
typedef uint8_t image_t[IMAGE_HEIGHT][IMAGE_WIDTH];

typedef struct
{
	pack_t weight0_1[LAYER0][LAYER1][HEIGHT_KERNEL][WIDTH_KERNEL];
	pack_t weight2_3[LAYER2][LAYER3][HEIGHT_KERNEL][WIDTH_KERNEL];
	pack_t weight4_5[LAYER4][LAYER5][HEIGHT_KERNEL][WIDTH_KERNEL];
	pack_t weight5_6[LAYER5 * WIDTH_FEATURE5 * HEIGHT_FEATURE5][OUTPUT];

	pack_t bias0_1[LAYER1];
	pack_t bias2_3[LAYER3];
	pack_t bias4_5[LAYER5];
	pack_t bias5_6[OUTPUT];

}CNN;


///卷积神经网络
void CNNPredict(const CNN *cnn, const image_t input, int result[SZPACK]);

#endif //CNN_H
