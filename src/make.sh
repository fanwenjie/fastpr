#!/bin/sh

NAME="fastpr.so"
if [ `uname -s` == "Darwin" ]; then
    NAME="fastpr.dylib"
fi
clang -O3 -mavx -shared -fPIC -std=c11 adaboost.c canny.c cnn.c detect.c fastpr.c utility.c -fvisibility=hidden -o $NAME

if [ -x "$NAME" ]; then
    strip -x $NAME
fi

