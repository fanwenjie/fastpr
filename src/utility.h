//
//  utility.h
//  FastPRDemo
//
//  Created by fanwenjie on 2017/6/3.
//  Copyright © 2017年 fanwenjie. All rights reserved.
//

#ifndef utility_h
#define utility_h

typedef struct {
    short x;
    short y;
    short width;
    short height;
}Rect;

void CropImage(const unsigned char *src, unsigned char *dst, short srcWidth, short dstWidth, short dstHeight, short x, short y);

void ImageResize(const unsigned char *srcImage, const short srcWidth,const short srcHeight,const short desWidth,const short desHeight,unsigned char *desImage);


void Dilate(unsigned char *src, unsigned char *dst, const int rows, const int cols);

#endif /* utility_h */
