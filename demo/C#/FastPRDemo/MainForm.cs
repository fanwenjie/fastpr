﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastPRDemo
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if(this.folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string dirPath = this.folderBrowserDialog.SelectedPath;
                var files = Directory.GetFiles(dirPath, "*.*", SearchOption.TopDirectoryOnly)
                    .Where(s => s.EndsWith(".jpg") || s.EndsWith(".png") || s.EndsWith(".bmp"));
                foreach (string file in files)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = new FileInfo(file).Name;
                    item.ToolTipText = file;
                    listView.Items.Add(item);
                }
            }
        }

        [DllImport("fastpr", EntryPoint = "FastprRecognize")]
        private static extern bool NativeFastprRecognize(byte[] image, short width, short height, short[] region, byte[] result);

        private bool FastprRecognize(Bitmap image, out Rectangle region, out string result)
        {
            short width = (short)image.Width, height = (short)image.Height;
            byte[] imgbuf = new byte[height * width];
            short[] regbuf = new short[4];
            byte[] resbuf = new byte[16];
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    Color c = image.GetPixel(j, i);
                    imgbuf[i * width + j] = (byte)(c.R * 76 + c.G * 150 + c.B * 30 >> 8);
                }
            }
            if (NativeFastprRecognize(imgbuf, width, height, regbuf, resbuf))
            {
                region = new Rectangle(regbuf[0], regbuf[1], regbuf[2], regbuf[3]);
                result = Encoding.Unicode.GetString(resbuf);
                return true;
            }
            else
            {
                region = new Rectangle();
                result = null;
                return false;
            }
        }

        private void listView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListViewItem item = e.Item;
            string path = item.ToolTipText;

            try
            {
                Bitmap bitmap = (Bitmap)Image.FromFile(path);
                pictureBox.Image = null;
                Rectangle region;
                string result = "识别失败";
                listView.Enabled = false;
                Thread thread = new Thread(() =>
                {
                    if (FastprRecognize(bitmap, out region, out result))
                    {
                        Color lineColor = Color.Red;
                        int left = region.X, top = region.Y, right = left + region.Width, bottom = top + region.Height;
                        for (int i = left; i <= right; ++i)
                        {
                            bitmap.SetPixel(i, top, lineColor);
                            bitmap.SetPixel(i, bottom, lineColor);
                        }
                        for (int i = top; i <= bottom; ++i)
                        {
                            bitmap.SetPixel(left, i, lineColor);
                            bitmap.SetPixel(right, i, lineColor);
                        }
                    }
                });
                thread.IsBackground = true;
                thread.Start();
                thread.Join();
                pictureBox.Image = bitmap;
                labelResult.Text = result;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            listView.Enabled = true;
        }
    }
}
