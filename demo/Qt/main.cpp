#include <QApplication>
#include <QDir>
#include <QLibrary>
#include <QMessageBox>

#include "mainwindow.h"

bool (*FastprRecognize)(const unsigned char *grayImage, const short width, const short height, short outRegion[4],unsigned short outResult[8]);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QDir::setCurrent(QApplication::applicationDirPath());
    QLibrary libfastpr("fastpr");
    if(libfastpr.load())
    {
        FastprRecognize = (bool(*)(const unsigned char *, const short, const short, short *,unsigned short *))libfastpr.resolve("FastprRecognize");
        if(FastprRecognize)
        {
            MainWindow w;
            w.show();
            return a.exec();
        }
    }
    QMessageBox::critical(0,"错误",libfastpr.fileName()+ "未找到！",QMessageBox::Ok);
    return 1;
}
