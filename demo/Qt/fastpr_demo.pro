#-------------------------------------------------
#
# Project created by QtCreator 2016-10-26T09:24:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fastpr_demo
TEMPLATE = app

windows{
    QMAKE_LFLAGS  += /STACK:8192000
}


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

DISTFILES += fastpr.so

