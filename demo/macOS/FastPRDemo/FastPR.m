//
//  FastPR.m
//  FastPRDemo
//
//  Created by fanwenjie on 2016/12/4.
//  Copyright © 2016年 fanwenjie. All rights reserved.
//


#import "FastPR.h"
#include <dlfcn.h>
#include <sys/time.h>

@implementation FastPR {
    BOOL (*_fastprRecognize)(const unsigned char *, const short, const short, short *,unsigned short *);
    void *_libfastpr;
}

- (instancetype)init {
    self = [super init];
    NSBundle *mainBundle = [NSBundle mainBundle];
    const char *libFastprPath = [[[mainBundle privateFrameworksPath]stringByAppendingString:@"/fastpr.dylib"]cStringUsingEncoding:NSUTF8StringEncoding];
    _libfastpr = dlopen(libFastprPath, RTLD_LAZY);
    if(_libfastpr) {
        _fastprRecognize = (BOOL(*)(const unsigned char *, const short, const short, short *,unsigned short *))dlsym(_libfastpr, "FastprRecognize");
        if(_fastprRecognize) {
            return self;
        }
    }
    return nil;
}

- (void)dealloc {
    if(_libfastpr)
        dlclose(_libfastpr);
}

- (NSString *)detectMultiScale:(NSBitmapImageRep *)imageRaw {
    if(imageRaw == nil)
        return nil;
    const NSUInteger width = imageRaw.pixelsWide;
    const NSUInteger height = imageRaw.pixelsHigh;
    unsigned char *grayImage = (unsigned char *)[[[NSMutableData alloc]initWithCapacity:width * height]bytes];
    for(int i = 0, k = 0; i < height; ++i)
        for(int j = 0; j<width; ++j)
        {
            CGFloat red,green,blue,alpha;
            [[imageRaw colorAtX:j y:i]getRed:&red green:&green blue:&blue alpha:&alpha];
            grayImage[k++] = (unsigned char)(red * 76 + green * 150 + blue * 30);
        }
    unsigned char (*colorImage)[4] = (unsigned char (*)[4])[[[NSMutableData alloc]initWithCapacity:width * height * 4]bytes];
    for(int i = 0, k = 0; i < height; ++i)
        for(int j = 0; j<width; ++j)
        {
            CGFloat red,green,blue,alpha;
            [[imageRaw colorAtX:j y:i]getRed:&red green:&green blue:&blue alpha:&alpha];
            colorImage[k][0] = (unsigned char)(blue * 255);
            colorImage[k][1] = (unsigned char)(green * 255);
            colorImage[k][2] = (unsigned char)(red * 255);
            k++;
        }
    struct timeval start,end;
    gettimeofday(&start,NULL);
    short v[4] = { 0 };
    unsigned short ch[8] = { 0 };
    char result = _fastprRecognize(grayImage, width, height, v, ch);
    if(!result){
        return nil;
    }
    NSString *ret = [[NSString alloc]initWithBytes:ch length:sizeof(ch) encoding:NSUTF16LittleEndianStringEncoding];
    gettimeofday(&end,NULL);
    Rect rect = { v[1] , v[0] , v[1] + v[3], v[0] + v[2]};
    NSColor *redColor = [NSColor redColor];
    for(NSUInteger i = rect.left;i < rect.right; ++i) {
        [imageRaw setColor:[NSColor redColor] atX:i y:rect.top];
        [imageRaw setColor:[NSColor redColor] atX:i y:rect.bottom];
    }
    for(NSUInteger i = rect.top;i < rect.bottom; ++i){
        [imageRaw setColor:redColor atX:rect.left y:i];
        [imageRaw setColor:redColor atX:rect.right y:i];
    }
    return ret;
}

@end
