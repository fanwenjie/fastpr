//
//  ViewController.m
//  FastPRDemo
//
//  Created by fanwenjie on 2016/12/5.
//  Copyright © 2016年 fanwenjie. All rights reserved.
//

#import "ViewController.h"
#import "FastPR.h"

@implementation ViewController {
    
    NSMutableArray *_array;
    
    FastPR *_fastPR;
    
    __weak IBOutlet NSTextField *_labelResult;
    
    __weak IBOutlet NSButton *_buttonOpenDir;
    
    __weak IBOutlet NSTableView *_tableView;
    
    __weak IBOutlet NSImageView *_imageView;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *view = (NSTableCellView *)[tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    NSString *filePath = [[_array objectAtIndex:row]description];
    view.textField.stringValue = [filePath lastPathComponent];
    return view;
}

- (IBAction)buttonClick:(id)sender {
    if(sender == _buttonOpenDir) {
        NSOpenPanel *openPanel = [NSOpenPanel openPanel];
        [openPanel setCanChooseFiles:NO];
        [openPanel setCanChooseDirectories:YES];
        if(NSFileHandlingPanelOKButton == [openPanel runModal]) {
            [_array removeAllObjects];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *dirPath = openPanel.URL.path;
            NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtPath:dirPath];
            NSString *fileName = nil;
            while(fileName = [enumerator nextObject]){
                NSString *extension = [fileName pathExtension];
                if([@"jpg" isEqualToString:extension] ||
                   [@"png" isEqualToString:extension] ||
                   [@"tiff" isEqualToString:extension]) {
                    fileName = [dirPath stringByAppendingFormat:@"/%@", fileName];
                    [_array addObject:fileName];
                }
            }
            [_tableView reloadData];
        }
    }
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_array count];
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    NSString *path = [[_array objectAtIndex:row]description];
    NSImage *image = [[NSImage alloc]initWithContentsOfFile:path];
    _imageView.image = image;
    _tableView.enabled = NO;
    _labelResult.stringValue = @"正在识别";
    NSBitmapImageRep *imageRaw = [NSBitmapImageRep imageRepWithData:[image TIFFRepresentation]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *result = [_fastPR detectMultiScale:imageRaw];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(result) {
                    _imageView.image = [[NSImage alloc]initWithCGImage:[imageRaw CGImage] size:(NSSize){imageRaw.pixelsWide,imageRaw.pixelsHigh}];
                    _labelResult.stringValue = result;
                }else {
                    _labelResult.stringValue = @"未识别出车牌";
                }
                _tableView.enabled = YES;
                
            });
    });    
    return YES;
}

- (void)loadView {
    [super loadView];
}

- (void) preferredContentSizeDidChangeForViewController:(NSViewController *)viewController {
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _array = [[NSMutableArray alloc]init];
    _fastPR = [[FastPR alloc]init];
    if(_fastPR == nil) {
        NSAlert *alert = [[NSAlert alloc]init];
        alert.messageText = @"加载识别模块失败";
        alert.alertStyle = NSAlertStyleCritical;
        [alert runModal];
        [NSApp terminate:self];
    }
    
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    // Update the view, if already loaded.
}


@end
