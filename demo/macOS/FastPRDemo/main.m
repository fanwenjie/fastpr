//
//  main.m
//  FastPRDemo
//
//  Created by fanwenjie on 2016/12/5.
//  Copyright © 2016年 fanwenjie. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
