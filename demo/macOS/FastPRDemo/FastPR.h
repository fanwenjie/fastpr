//
//  FastPR.h
//  FastPRDemo
//
//  Created by fanwenjie on 2016/12/4.
//  Copyright © 2016年 fanwenjie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
@interface FastPR : NSObject

-(NSString *)detectMultiScale:(NSBitmapImageRep *)imageRaw;

@end
